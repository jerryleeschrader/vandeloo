<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php wp_title(); ?></title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
      <!-- Begin sticky footer, ends in footer.php -->
  <div id="wrap">
  <section id="top">
    <div class="container">
      <div class="row">
      <div class="col-sm-3">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" alt="Personal Injury Lawyer"></a>
        </div>
        <div class="col-md-4 col-lg-4 hidden-xs hidden-sm"><h4 class="text-center">FREE CONSULTATION <br /><span>NO CHARGE UNLESS YOU WIN</span></h4></div>
        <div class="col-sm-8 col-md-4 col-lg-4"><h3 class="pull-right"><strong>PHONE:</strong> 410-321-6200<br /><strong>HOTLINE:</strong> 443-414-1105</h3></div>
      </div>
    </div>
  </section><!--/top-->
  <section id="navigation">
    <div class="container">
      <div class="row">
        <?php get_template_part('partials/menu'); ?>
      </div>
    </div>
  </section><!--/navigation-->