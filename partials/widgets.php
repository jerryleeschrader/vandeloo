<?php

function create_widgets() {

	register_sidebar(array(
   'name' => __('Main Sidebar'),
   'id' => 'main-sidebar',
   'before_widget' => '',
   'after_widget' => '',
   'before_title' => '<h3>',
   'after_title' => '</h3>'
    ));

   register_sidebar(array(
   'name' => __('Footer Sidebar One'),
   'id' => 'footer-sidebar-one',
   'before_widget' => '',
   'after_widget' => '',
   'before_title' => '<h3>',
   'after_title' => '</h3>'
   ));

   register_sidebar(array(
   'name' => __('Footer Sidebar Two'),
   'id' => 'footer-sidebar-two',
   'before_widget' => '',
   'after_widget' => '',
   'before_title' => '<h3>',
   'after_title' => '</h3>'
   ));

   register_sidebar(array(
   'name' => __('Footer Sidebar Three'),
   'id' => 'footer-sidebar-three',
   'before_widget' => '',
   'after_widget' => '',
   'before_title' => '<h3>',
   'after_title' => '</h3>'
   ));

}
add_action('widgets_init','create_widgets');