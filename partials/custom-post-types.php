<?php

// Register Testimonial Post Type
function testimonials_post_type() {

	$labels = array(
		'name'                => _x( 'Testimonials', 'Post Type General Name', 'vandeloo' ),
		'singular_name'       => _x( 'Testimonial', 'Post Type Singular Name', 'vandeloo' ),
		'menu_name'           => __( 'Testimonial', 'vandeloo' ),
		'name_admin_bar'      => __( 'Testimonial', 'vandeloo' ),
		'parent_item_colon'   => __( 'Parent Testimonial:', 'vandeloo' ),
		'all_items'           => __( 'All Testimonials', 'vandeloo' ),
		'add_new_item'        => __( 'Add New Testimonial', 'vandeloo' ),
		'add_new'             => __( 'Add Testimonial', 'vandeloo' ),
		'new_item'            => __( 'New Testimonial', 'vandeloo' ),
		'edit_item'           => __( 'Edit Testimonial', 'vandeloo' ),
		'update_item'         => __( 'Update Testimonial', 'vandeloo' ),
		'view_item'           => __( 'View Testimonial', 'vandeloo' ),
		'search_items'        => __( 'Search Testimonial', 'vandeloo' ),
		'not_found'           => __( 'Not found', 'vandeloo' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'vandeloo' ),
	);
	$args = array(
		'label'               => __( 'Testimonial', 'vandeloo' ),
		'description'         => __( 'Testimonials', 'vandeloo' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-smiley',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'testimonials', $args );

}
add_action( 'init', 'testimonials_post_type', 0 );