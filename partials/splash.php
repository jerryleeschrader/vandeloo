<div class="container">
	<div class="row clearfix">
		<div class="col-md-6 col-lg-5">
			<img src="<?php echo get_template_directory_uri(); ?>/images/amy-vandeloo.png" alt="Amy Vandeloo Personal Injury Lawyer" class="splash-image-left">
		</div>
				<div class="row-fluid hidden-xs hidden-sm">
				<h3 class="one">Have You Been Injured?</h3>
			</div>
			<div class="row-fluid hidden-xs hidden-sm">
				<h3 class="two">Get The Money You Deserve!</h3>
			</div>
			<div class="row-fluid hidden-xs hidden-sm">
				<h3 class="three">
					<a href="#" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">Get Help Now!</a>
				</h3>
			</div>
	</div><!--/row-->
</div>