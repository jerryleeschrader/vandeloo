	<div class="row-fluid">
		<div class="col-sm-12" style="padding-left:0;">
			<div id="sliding-container">
			<figure>
				<img src="<?php the_field('picture_image'); ?>" alt="<?php the_field('picture_title'); ?>">
				<figcaption><?php the_field('picture_title'); ?></figcaption>
			</figure>
		</div>
		</div>
	</div>