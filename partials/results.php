<div class="carousel-reviews broun-block">
    <div class="container">
        <div class="row">
        <h3 class="text-center">See What Our Clients Say About Us</h3>
            <div id="carousel-reviews" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
<?php $first = true; 
      $colCount = 1;
      $currentPost=1;
      query_posts('post_type=testimonials&posts_per_page=-1');
      global $wp_query; $postCount = $wp_query->found_posts;
      while ( have_posts() ) : the_post(); ?>
<?php if($colCount == 1): ?>
                    <div class="item <?php if($first) echo 'active'; $first = false;?>">
<?php endif; ?>
                      <div class="col-md-4 col-sm-6">
                    <div class="block-text rel zmin">
                    <a title="" href="<?php the_permalink(); ?>" class="review-title"><?php the_title(); ?></a>
                  <div class="mark">Rating: 
                  <span class="rating-input">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </span>
                  </div>
                    <p><?php echo wp_trim_words( get_the_content(), 20, '...' ); ?></p>
                  </div>
            </div><!-- /item -->
<?php if($colCount == 3 || $currentPost == $postCount): $colCount =0; ?>
              </div>
<?php endif; 
        $colCount++;
        $currentPost++;?>
<?php endwhile;
wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
