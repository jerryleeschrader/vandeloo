<div class="container">
	<div class="row">
	<h3 class="text-center">Featured News</h3>
	<?php
		$args = array(
			'category' => '84',
			'posts_per_page' => '3',
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<div class="col-sm-4">
			<a href="<?php the_permalink(); ?>">
			<div class="inner-container">
				<h3><?php the_title(); ?></h3>
				<p><?php echo wp_trim_words( get_the_content(), 50, '...' ); ?></p>
				<small>by <?php the_author(); ?></small>
				<div class="bottom-box"><i class="fa fa-chevron-circle-right"></i> Read More At Our blog</div>
			</div><!--/inner-container-->
			</a>
		</div><!--/col-sm-4-->
	<?php endforeach; wp_reset_postdata(); ?>
	</div><!--/row-->
</div><!--/container-->