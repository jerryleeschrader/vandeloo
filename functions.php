<?php

include('partials/custom-post-types.php');
include('partials/widgets.php');

function load_styles() {
	wp_enqueue_style('bootstrap_styles', get_template_directory_uri() . '/css/bootstrap.css');
	wp_enqueue_style('wordpress_styles', get_template_directory_uri() . '/css/wordpress.css');
	wp_enqueue_style('fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
	wp_enqueue_style('google_fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic');
	wp_enqueue_style('animate_styles',  get_template_directory_uri() . '/css/animate.css');
	wp_enqueue_style('custom_styles',  get_template_directory_uri() . '/styles.css');
}
add_action('wp_enqueue_scripts', 'load_styles');

function load_scripts() {
	wp_enqueue_script('bootstrap_scripts', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'), '1.0', true);
	wp_enqueue_script('custom_scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0', true);
}
add_action('wp_enqueue_scripts', 'load_scripts');

// navigation
require_once('wp_bootstrap_navwalker.php');

register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'spector' ),
) );

function new_excerpt_more( $more ) {
	return '';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );