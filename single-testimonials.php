<?php get_header(); ?>
<section id="breadcrumb">
	<?php get_template_part('partials/breadcrumbs'); ?>
</section>
<section id="main-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 reviews-content">
			<h1>Review by: <?php the_title(); ?></h1>
				<?php if(have_posts()):while(have_posts()):the_post(); ?>
				<div itemscope itemtype="http://schema.org/Review">
<div itemprop="name"><strong><?php the_title(); ?> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></strong></div>
<p>&nbsp;</p>
<div itemprop="description"><strong>Case Type: <?php echo get_post_meta($post->ID, 'type', true); ?></strong></div>
<div itemprop="reviewBody"><?php the_content(); ?></div>
<div itemprop="author" itemscope itemtype="http://schema.org/Person">
<strong>Written by:</strong> <span itemprop="name"><?php the_title(); ?></span></div>
<div itemprop="itemReviewed" itemscope itemtype="http://schema.org/Thing">
<span itemprop="name"><strong>Reviewed by:</strong> </span><?php the_title(); ?></div>
<div><meta itemprop="datePublished" content="2015-09-08"><strong>Date published:</strong> <?php echo get_post_meta($post->ID, 'date', true); ?></div>
<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
<meta itemprop="worstRating" content="1"><span itemprop="ratingValue">5</span> / <span itemprop="bestRating">5</span> stars</div>
<hr />
				<?php endwhile; endif; ?>
				</div><!--/schema itemscope -->
			</div><!--/col-sm-8-->
			<div class="col-sm-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>