<?php get_header();  ?>
<section id="breadcrumb">
	<?php get_template_part('partials/breadcrumbs'); ?>
</section>
<section id="main-content">
	<div class="container">
		<div class="row">

			<div class="col-sm-8">
			<h1><?php the_title(); ?></h1>
			<?php get_template_part('partials/page-pictures'); ?>
				<?php if(have_posts()):while(have_posts()):the_post(); ?>
				<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
			<div class="col-sm-4">
				<?php get_sidebar(); ?>
			</div>

		</div>
	</div><!--/container-->
</section>
<?php get_footer(); ?>
