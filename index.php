<?php get_header();  ?>
<div class="container">
	<div class="row">
		<div class="col-sm-8">
		<h1>The VanDeLoo Firm Blog</h1>
			<?php if(have_posts()):while(have_posts()):the_post(); ?>
				<div class="row clearfix">
					<div class="col-sm-3" style="padding-top:50px;">
						<?php 
						if (has_post_thumbnail()) {
							the_post_thumbnail( 'thumbnail' );
						} else { ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/amy-thumbnail.jpg" alt="Amy VanDeLoo Injury Lawyer">
						<?php }
						?>
					</div>
					<div class="col-sm-9">
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<?php the_excerpt(); ?>
					</div>
				</div><!--/row -->
				<hr />
				<?php endwhile; ?>
					<div class="row-fluid pagination">
						<div class="nav-previous alignleft"><i class="fa fa-2x fa-chevron-circle-left"></i> <?php next_posts_link( 'Older posts' ); ?></div>
						<div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?> <i class="fa fa-2x fa-chevron-circle-right"></i></div>
					</div>
				<?php endif; ?>
		</div>
		<div class="col-sm-4">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
