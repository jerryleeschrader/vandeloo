</div><!--/sticky footer, begins in header.php -->
<section id="results">
	<?php get_template_part('partials/latest-news'); ?>
</section>
<section id="upper-footer">
	<?php get_template_part('partials/results'); ?>
</section>
<section id="upper-footer-widgets">
	<div class="container">
		<div class="row">
			<div class="col-sm-4"><?php dynamic_sidebar('footer-sidebar-one'); ?></div>
			<div class="col-sm-4"><?php dynamic_sidebar('footer-sidebar-two'); ?></div>
			<div class="col-sm-4"><?php dynamic_sidebar('footer-sidebar-three'); ?></div>
		</div>
	</div>
</section>
<footer id="footer">
	<div class="container">
		<div class="row">
			&copy; <?php echo date('Y') ?> The VanDeLoo Firm LLC &middot; All Rights Reserved.
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
<?php get_template_part('partials/modals'); ?>
</body>
</html>