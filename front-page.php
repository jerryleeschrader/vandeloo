<?php get_header();  ?>
<section id="splash">
	<?php get_template_part('partials/splash'); ?>
</section>
<section id="splash-bar">
	<?php get_template_part('partials/splash-bar'); ?>
</section>
<section id="main-content">
	<div class="container">
		<div class="row">		
			<div class="col-sm-8">
			<h1>Personal Injury Lawyer in Dallas</h1>
			<?php get_template_part('partials/page-pictures'); ?>
				<?php if(have_posts()):while(have_posts()):the_post(); ?>
				<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
			<div class="col-sm-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div><!--/container -->
</section>
<?php get_footer(); ?>
