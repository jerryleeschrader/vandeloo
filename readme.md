# The VanDeLoo Firm Website (Theme for Wordpress)
## Responsive theme built on Bootstrap 3 using Less, assets folder holding Less and JS files, compiled with Codekit

Custom designed and coded by [Jerry Schrader](http://www.jerryschrader.info) while working at [Exclusive Legal Marketing](http://www.exclusivelegalmarketing.com) for [The VanDeLoo Firm](http://www.vandeloofirm.com)