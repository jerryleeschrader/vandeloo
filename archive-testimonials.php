<?php get_header(); ?>
<section id="breadcrumb">
	<?php get_template_part('partials/breadcrumbs'); ?>
</section>
<section id="main-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 reviews-content">
			<h1>HEAR FROM OUR CLIENTS</h1>
				<?php 
				$args = array(
				'post_type' => 'testimonials',
				);
				$query = new WP_Query($args);

				if(have_posts()):while(have_posts()):the_post(); ?>
				<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h4>
				<p><strong>Case Type:</strong> <?php echo get_post_meta($post->ID, 'type', true); ?></p>
				<p><?php the_excerpt(); ?></p>
				<hr />
				<?php endwhile; endif; ?>
			</div>
			<div class="col-sm-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>